let
  configPath = ./config;

  user0 = "appoota";
in
{
  inherit
    user0
    ;
  users = {
    "${user0}" = import (configPath + "/user0.nix") {
      inherit
        user0
        ;
    };
  };
}
