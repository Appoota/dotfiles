{ user0 }:
let
  inherit
    user0
    ;
in
{
  name = "Appoota";
  aliases = {

  };
  email = {
    address0 = "";

  };
  paths = {
    path0 = ""; # Git path
  };
  sshKeys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFQslbhiMt2JiCCg0UWT/rvmfpmISMfefztXqR+BpNOw appoota@NixOS"
  ];
}
