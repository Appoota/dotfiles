let
  currentTheme = catppuccin-mocha;

  brogrammer = "brogrammer";
  catppuccin-frappe = "catppuccin-frappe";
  catppuccin-latte = "catppuccin-latte";
  catppuccin-macchiato = "catppuccin-macchiato";
  catppuccin-mocha = "catppuccin-mocha";
  chalk = "chalk";
  deep-oceanic-next = "deep-oceanic-next";
  dracula = "dracula";
  espresso = "espresso";
  flat = "flat";
  framer = "framer";
  github = "github";
  hardcore = "hardcore";
  one-black = "one-black";
  one-dark = "one-dark";
  one-light = "one light";
  sparky = "sparky";

in
{
  themes = {
    currentTheme = currentTheme;
    cursor = {
      name = "";
      size = 18;
    };
    font = {
      name = "MonaspiceRn Nerd Font";
      size = {

        applications = 14;
        desktop = 12;
        popups = 10;
        terminal = 10;

      };
    };
    palettes =
      let
        palettePath = ./palettes;
        catppuccinPath = /catppuccin;
        onePath = /one;
      in
      {
        ${brogrammer} = import (palettePath + /brogrammer);
        ${catppuccin-frappe} = import (palettePath + catppuccinPath + /frappe);
        ${catppuccin-latte} = import (palettePath + catppuccinPath + /latte);
        ${catppuccin-macchiato} = import (palettePath + catppuccinPath + /macchiato);
        ${catppuccin-mocha} = import (palettePath + catppuccinPath + /mocha);
        ${chalk} = import (palettePath + /chalk);
        ${deep-oceanic-next} = import (palettePath + /deep-oceanic-next);
        ${dracula} = import (palettePath + /dracula);
        ${espresso} = import (palettePath + /espresso);
        ${flat} = import (palettePath + /flat);
        ${framer} = import (palettePath + /framer);
        ${github} = import (palettePath + /github);
        ${hardcore} = import (palettePath + /hardcore);
        ${one-black} = import (palettePath + onePath + /black);
        ${one-dark} = import (palettePath + onePath + /dark);
        ${one-light} = import (palettePath + onePath + /light);
        ${sparky} = import (palettePath + /sparky);

      };
  };
}
