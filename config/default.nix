{ lib, ... }:
let
  stringType = lib.mkOption {
    type = lib.types.str;
  };
  intType = lib.mkOption {
    type = lib.types.int;
  };
  listType = lib.mkOption {
    type = lib.types.listOf lib.types.str;
  };
  attrList = lib.mkOption {
    type = lib.types.attrsOf lib.types.str;
  };

  numOptions = 20;

  genOptions =
    config: prefix:
    builtins.listToAttrs (
      map (i: {
        name = "${prefix}${toString i}";
        value = config;
      }) (builtins.genList (i: i) numOptions)
    );

  mkOptionsFromDir =
    path:
    builtins.listToAttrs (
      map
        (name: {
          name = builtins.substring 0 (builtins.stringLength name - 4) name;
          value = stringType;
        })
        (
          builtins.filter (name: builtins.match ".*\\.nix$" name != null) (
            builtins.attrNames (builtins.readDir path)
          )
        )
    );

  userSubmodule = lib.types.submodule {
    options = {
      name = stringType;
      label = stringType;
      sshKeys = listType;
      group = stringType;
      aliases = genOptions stringType "name";
      email = genOptions stringType "address";
      paths = genOptions stringType "path";
    };
  };

  themesSubmodule = lib.types.submodule {
    options = {
      currentTheme = stringType;
      font = {
        name = stringType;
        size = {
          applications = intType;
          desktop = intType;
          popups = intType;
          terminal = intType;
        };
      };
      cursor = {
        name = stringType;
        size = intType;
      };
      palettes = lib.mkOption {
        type = lib.types.attrsOf (
          lib.types.submodule {
            options = {
              colours = attrList;
              font = stringType;
            };
          }
        );
      };

    };
  };
in
{
  options = {

    people = lib.mkOption {
      type = lib.types.submodule {
        options = mkOptionsFromDir ./users/config // {
          users = lib.mkOption {
            type = lib.types.attrsOf userSubmodule;
          };
        };
      };
    };
    aesthetics = lib.mkOption {
      type = lib.types.submodule {
        options = mkOptionsFromDir ./themes // {
          themes = lib.mkOption {
            type = themesSubmodule;
          };
        };
      };
    };
  };

  config = {
    aesthetics = import ./themes;
    people = import ./users;
  };
}
