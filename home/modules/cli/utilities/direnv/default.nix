{
  flake,
  pkgs,
  ...
}:
{
  programs.direnv = {
    enable = true;
    package = flake.inputs.direnv.packages.${pkgs.system}.default;
    nix-direnv.enable = true;
  };
}
