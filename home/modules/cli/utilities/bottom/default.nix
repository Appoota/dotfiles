{ flake, ... }:
{
  programs.bottom =
    let
      configPath = ./config;
      settingsPath = import (configPath + /settings.nix) {
        inherit
          flake
          ;
      };
    in
    {
      enable = true;
      settings = settingsPath;
    };
}
