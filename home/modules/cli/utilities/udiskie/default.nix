{
  services.udiskie = {
    enable = true;
    automount = true;
    tray = "always";
  };
}
