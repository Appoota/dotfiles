{
  flake,
  lib,
  ...
}:
let
  configPath = ./config;
  settingsPath = import (configPath + /settings.nix) {
    inherit
      flake
      lib
      ;
  };
in
{
  programs.starship = {
    enable = true;
    settings = settingsPath;
  };
}
