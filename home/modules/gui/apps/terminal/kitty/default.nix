{
  flake,
  ...
}:
let
  inherit (flake.config.aesthetics.themes)
    font
    ;
in
{
  programs.kitty = {
    enable = true;
    font = {
      name = font.name;
      size = font.size.terminal;
    };
    themeFile = "Catppuccin-Macchiato";
  };
}
