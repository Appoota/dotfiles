{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes)
    font
    ;
in
''
  return {
    color_scheme = "Catppuccin Macchiato",
    font_size = ${builtins.toString font.size.terminal},
    font = wezterm.font('${font.name}'),
    enable_tab_bar = false,
    window_close_confirmation = 'NeverPrompt',
    term = 'wezterm',
    enable_wayland = true,
    front_end = "WebGpu",
    max_fps = 200
  }
''
