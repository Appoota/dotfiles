{
  flake,
  pkgs,
  ...
}:
let
  inherit (flake.config.aesthetics.themes)
    font
    ;
in
{
  confirm-close-surface = false;
  window-decoration = false;
  font-size = font.size.terminal;
  font-family = font.name;
  window-padding-x = 10;
  window-padding-y = 10;
  copy-on-select = true;
  bold-is-bright = true;
  shell-integration = "detect";
  command = "${pkgs.nushell}/bin/nu";
}
