{
  flake,
  pkgs,
  ...
}:
let
  configPath = ./config;
  settingsPath = import (configPath + /settings.nix) {
    inherit
      flake
      pkgs
      ;
  };
  themesPath = import (configPath + /themes.nix) {
    inherit
      flake
      ;
  };
in
{
  programs.ghostty = {
    enable = true;
    package = flake.inputs.ghostty.packages.${pkgs.system}.default;
    settings = settingsPath;
    themes = themesPath;
  };
}
