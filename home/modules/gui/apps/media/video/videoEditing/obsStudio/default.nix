{ pkgs, ... }:
{
  programs = {
    obs-studio = {
      enable = true;
      plugins = builtins.attrValues {
        inherit (pkgs.obs-studio-plugins)
          obs-tuna
          obs-vkcapture
          obs-multi-rtmp
          ;
      };
    };
  };
  home = {
    file = {
      "./.config/obs-studio/themes" = {
        source = ./themes;
      };
    };
  };
}
