[
  {
    name = "Bookmarks";
    toolbar = true;
    bookmarks = [
      # insert your bookmarks here
      #{
      # name = "Google";
      # url = "https://www.google.com/";
      # tags = [""];
      # keyword = "Google";
      #}
      {
        name = "Whatsapp";
        url = "https://web.whatsapp.com";
        keyword = "Whatsapp";
      }
      {
        name = "Instagram";
        url = "https://www.instagram.com/";
        keyword = "Instagram";
      }
      {
        name = "Twitter";
        url = "https://twitter.com/home";
        keyword = "Twitter";
      }
      {
        name = "Youtube";
        url = "https://www.youtube.com";
        tags = [
          "yt"
          "youtube"
        ];
        keyword = "youtube";
      }
      {
        name = "Wikipedia";
        url = "https://www.wikipedia.org";
        tags = [
          "wikipedia"
          "wiki"
        ];
        keyword = "wiki";
      }
      {
        name = "IEP";
        url = "https://iep.utm.edu/";
        tags = [
          "iep"
          "philosophy"
        ];
        keyword = "iep";
      }
      {
        name = "Scholar";
        url = "https://scholar.google.com";
        tags = [ "scholar" ];
        keyword = "scholar";
      }
      {
        name = "Tree Proof";
        url = "https://www.umsu.de/trees/";
        tags = [
          "proof"
          "tree proof"
        ];
        keyword = "tree proof";
      }
      {
        name = "Scihub";
        url = "https://www.sci-hub.st/";
        tags = [
          "sh"
          "scihub"
        ];
        keyword = "scihub";
      }
      {
        name = "csTimer";
        url = "https://cstimer.net/";
        tags = [
          "cs"
          "timer"
        ];
        keyword = "cstimer";
      }
      {
        name = "DeTeXify";
        url = "https://detexify.kirelabs.org/classify.html";
        tags = [
          "latex"
          "symbol"
        ];
        keyword = "DeTeXify";
      }
    ];
  }
]
