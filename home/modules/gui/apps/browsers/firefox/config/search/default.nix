{
  force = true;
  # set your default search engine
  default = "DuckDuckGo";
  engines = {
    # insert your custom search engines here
    # "Google" = {
    #   definedAliases = ["@g"];
    #   icon = ./icons/google.png;
    #   urls = [{template = "https://www.google.com/?k={searchTerms}";}];
    "Wikipedia" = {
      definedAliases = [ "@w" ];
      icon = ./icons/wi.png;
      urls = [ { template = "https://en.wikipedia.org/wiki/{searchTerms}"; } ];
    };

    "Stanford Encyclopedia of Philosophy" = {
      definedAliases = [ "@se" ];
      icon = ./icons/ph.png;
      urls = [ { template = "https://plato.stanford.edu/search/search?query={searchTerms}"; } ];
    };

    "Nixpkgs Search" = {
      definedAliases = [ "@npp" ];
      icon = ./icons/nx.png;
      urls = [
        {
          template = "https://search.nixos.org/packages?channel=unstable&size=50&sort=relevance&type=packages&query={searchTerms}";
        }
      ];
    };

    # hidden default search engines
    "Amazon.in".metaData.hidden = true;
    "Bing".metaData.hidden = true;
    "eBay".metaData.hidden = true;
    "Wikipedia (en)".metaData.hidden = true;
  };
  order = [
    # order of search engines in the address bar
    #"Google"
  ];
}
