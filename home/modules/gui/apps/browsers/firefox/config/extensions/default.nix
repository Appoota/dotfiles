{ pkgs, ... }:
{
  extensions = builtins.attrValues {
    inherit (pkgs.nur.repos.rycee.firefox-addons)
      # bitwarden
      # enhancer-for-youtube
      # sponsorblock
      # ublock-origin
      # unpaywall
      # sidebery
      ;
  };
}
