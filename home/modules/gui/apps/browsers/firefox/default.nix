{
  pkgs,
  flake,
  ...
}:
let
  inherit (flake.config.people)
    user0
    ;
in
{
  programs.firefox =
    let
      configPath = ./config;
      bookmarksPath = import (configPath + /bookmarks);
      extensionsPath = import (configPath + /extensions) {
        inherit
          pkgs
          ;
      };
      searchPath = import (configPath + /search);
      settingsPath = import (configPath + /settings);
      themesPath = import (configPath + /themes);
    in
    {
      enable = true;
      package = pkgs.firefox;
      profiles = {
        ${user0} = {
          isDefault = true;
          id = 0;
          bookmarks = bookmarksPath;
          search = searchPath;
          settings = settingsPath;
          # extensions = [ extensionsPath ];
        } // themesPath;
      };
    };
}
