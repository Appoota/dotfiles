{
  lib,
  pkgs,
  flake,
  ...
}:
let
  inherit (flake.config.aesthetics.themes)
    font
    ;
in
{
  "git.confirmSync" = false;
  "editor.insertSpaces" = false;
  "files.autoSave" = "afterDelay";
  "git.enableSmartCommit" = true;
  "nix.enableLanguageServer" = true;
  "nix.serverPath" = lib.getExe pkgs.nil;
  "nix.formatterPath" = lib.getExe pkgs.haskellPackages.nixfmt;
  "window.menuBarVisibility" = "toggle";
  "workbench.iconTheme" = "material-icon-theme";
  "workbench.startupEditor" = "none";
  "workbench.colorTheme" = "Catppuccin Macchiato";
  "workbench.statusBar.visible" = false;
  "editor.multiCursorLimit" = 700000;
  "elmLS.disableElmLSDiagnostics" = true;
  "elmLS.elmReviewDiagnostics" = "warning";
  "editor.wordWrap" = "on";
  "editor.fontSize" = font.size.applications;
  "editor.fontFamily" = "'${font.name}', 'monospace', monospace";
  "terminal.integrated.fontSize" = font.size.applications;
  "terminal.integrated.fontFamily" = "'${font.name}', 'monospace', monospace";
  "editor.fontLigatures" = true;
}
