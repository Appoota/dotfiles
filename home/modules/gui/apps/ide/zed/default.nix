{
  flake,
  lib,
  pkgs,
  ...
}:
let
  configPath = ./config;
  extensionsPath = import (configPath + /extensions);
  userKeymapsPath = import (configPath + /userKeymaps);
  userSettingsPath = import (configPath + /userSettings) {
    inherit
      flake
      lib
      pkgs
      ;
  };
  extraPackagesPath = import (configPath + /extraPackages) {
    inherit
      pkgs
      ;
  };
in
{
  programs.zed-editor = {
    enable = true;
    extraPackages = extraPackagesPath;
    extensions = extensionsPath;
    userKeymaps = userKeymapsPath;
    userSettings = userSettingsPath;
  };
}
