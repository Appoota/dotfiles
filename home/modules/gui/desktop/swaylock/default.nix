{ flake, ... }:
let
  configPath = ./config;
  settingsPath = import (configPath + /settings.nix) {
    inherit
      flake
      ;
  };
in
{
  programs.swaylock = {
    enable = true;
    settings = settingsPath;
  };
}
