{ pkgs, ... }:
{
  general = {
    lock_cmd = "pidof swaylock || swaylock";
    before_sleep_cmd = "loginctl lock-session";
  };
  listener = [
    {
      timeout = 150;
      on-timeout = "${pkgs.brightnessctl}/bin/brightnessctl -s set 0";
      on-resume = "${pkgs.brightnessctl}/bin/brightnessctl -r";
    }
    {
      timeout = 150;
      on-timeout = "${pkgs.brightnessctl}/bin/brightnessctl -sd rgb:kbd_backlight set 0";
      on-resume = "${pkgs.brightnessctl}/bin/brightnessctl -rd rgb:kbd_backlight";
    }
    {
      timeout = 300;
      on-timeout = "systemctl suspend";
    }
  ];
}
