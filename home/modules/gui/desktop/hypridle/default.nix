{
  flake,
  pkgs,
  ...
}:
let
  configPath = ./config;
  settingsPath = import (configPath + /settings.nix) {
    inherit
      pkgs
      ;
  };
in
{
  services.hypridle = {
    enable = true;
    package = flake.inputs.hypridle.packages.${pkgs.system}.hypridle;
    settings = settingsPath;
  };
}
