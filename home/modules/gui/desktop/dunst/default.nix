{ flake, ... }:
let
  configPath = ./config;
  settingsPath = import (configPath + /settings.nix) {
    inherit
      flake
      ;
  };
in
{
  services.dunst = {
    enable = true;
    settings = settingsPath;
  };
}
