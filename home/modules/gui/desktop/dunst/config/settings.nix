{ flake, ... }:
let

  inherit (flake.config.aesthetics.themes)
    currentTheme
    palettes
    font
    ;

  el = palettes.${currentTheme}.colours;

  makeColor = c: "#" + c;
in
{
  global = {
    font = "${font.name} ${builtins.toString font.size.popups}";
    background = makeColor el.base01;
    frame_color = makeColor el.base0E;
    foreground = makeColor el.base05;
    corner_radius = 10;
    fade_in_duration = 1000;
    frame = 10000;
    frame_width = 1;
    icon_corner_radius = 10;
    monitor = 1;
    offset = "20x20";
    origin = "bottom-right";
    progress_bar_corner_radius = 4;
    timeout = 10;
    transparecncy = true;
  };

  urgency_critical = {
    frame_color = makeColor el.base09;
    timeout = 0;
  };
}
