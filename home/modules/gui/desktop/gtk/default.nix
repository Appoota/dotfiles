{ pkgs, flake, ... }:
let
  configPath = ./config;
  cursorThemePath = import (configPath + /cursorTheme.nix) {
    inherit
      flake
      pkgs
      ;
  };
  iconThemePath = import (configPath + /iconTheme.nix) {
    inherit
      pkgs
      ;
  };
  themePath = import (configPath + /theme.nix) {
    inherit
      pkgs
      ;
  };
  homeCursorPath = import (configPath + /homeCursor.nix) {
    inherit
      flake
      pkgs
      ;
  };
  gtk3Path = import (configPath + /gtk3.nix);
in
{
  gtk = {
    enable = true;
    gtk3 = gtk3Path;
    cursorTheme = cursorThemePath;
    iconTheme = iconThemePath;
    theme = themePath;
  };
  home.pointerCursor = homeCursorPath;
}
