{
  pkgs,
  flake,
  ...
}:
let
  inherit (flake.config.aesthetics.themes) cursor;
in
{
  name = "catppuccin-mocha-dark-cursors";
  package = pkgs.catppuccin-cursors.mochaDark;
  size = cursor.size;
}
