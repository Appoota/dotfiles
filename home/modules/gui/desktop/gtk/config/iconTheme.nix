{ pkgs, ... }:
{
  package = pkgs.catppuccin-papirus-folders.override {
    flavor = "mocha";
    accent = "blue";
  };
  name = "Papirus-Dark";
}
