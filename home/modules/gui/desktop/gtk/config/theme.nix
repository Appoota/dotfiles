{ pkgs, ... }:
{
  package = pkgs.catppuccin-gtk.override {
    accents = [ "blue" ];
    size = "standard";
    variant = "mocha";
  };
  name = "catppuccin-mocha-blue-compact";
}
