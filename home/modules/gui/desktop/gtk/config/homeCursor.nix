{ pkgs, flake, ... }:
let
  inherit (flake.config.aesthetics.themes)
    cursor
    ;
in
{
  gtk.enable = true;
  x11.enable = true;
  name = "catppuccin-mocha-dark-cursors";
  package = pkgs.catppuccin-cursors.mochaDark;
  size = cursor.size;
}
