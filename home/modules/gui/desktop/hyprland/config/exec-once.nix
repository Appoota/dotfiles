{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes)
    cursor
    ;
in
[
  "swaylock"
  "wl-clipboard"
  "wpaperd"
  "gammastep"
  "dunst"
  "playerctrld"
  "waybar"
  "nm-applet"
  "blueman-applet"
  "easyeffects -r"
  "emote"
  "flameshot"
  "gsettings set org.gnome.desktop.interface cursor-theme 'catppuccin-mocha-dark-cursors'"
  "gsettings set org.gnome.desktop.interface cursor-size ${builtins.toString cursor.size}"
]
