{
  enabled = true;
  animation = [
    "border, 1, 2.2, bounce"
    "fadeIn, 1, 1, bounce"
    "fadeOut, 1, 4.5, bounce"
    "fadeSwitch, 1, 2.2, bounce"
    "windows, 1, 2.2, bounce"
    "windowsOut, 1, 1.1, bounce"
    "workspaces, 1, 2.2, bounce, slide"
  ];
  bezier = [
    "wind, 0, 1, 0.5, 1"
    "winIn, 0, 0.5, 0, 1"
    "winOut, 0, 0.5, 0, 1"
    "bounce, 0.7, 1.7, 0.5, 1"
  ];
}
