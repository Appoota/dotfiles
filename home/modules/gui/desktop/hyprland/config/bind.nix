let
  superBinds = builtins.map (x: "SUPER, " + x) [
    # Apps
    "Space, exec, rm -r /home/$USER/.cache/tofi* ; tofi-drun"
    "C, exec, zed"
    "A, exec, obsidian"
    "D, exec, ghostty -e zellij a dotfiles"
    "E, exec, ghostty -e nu -e y"
    "T, exec, ghostty"
    "B, exec, firefox"
    "V, exec, vesktop"
    "W, exec, bitwarden"
    "H, exec, feishin"
    "Y, exec, scrcpy"
    "R, exec, ghostty -e nu -e btm"
    "N, exec, signal-desktop"
    "M, exec, element-desktop"
    "Home, exec, sudo protonvpn c --cc CA"
    "End, exec, hyprctl dispatch exit"
    "S, exec, steam"
    "period, exec, emote"
    # Workspaces
    "1, workspace, 1"
    "2, workspace, 2"
    "3, workspace, 3"
    "4, workspace, 4"
    "5, workspace, 5"
    # Windows
    "F, togglefloating"
    "X, killactive"
    "Tab, fullscreen, 0"
    "P, pin, enable"
    "equal, splitratio, 0.33"
    "minus, splitratio, -0.33"
    # Window Focus
    "left , movefocus, l"
    "down , movefocus, d"
    "up , movefocus, u"
    "right , movefocus, r"
    # Audio
    "F8, exec, playerctl next"
    "F7, exec, playerctl previous"
    "F6, exec, playerctl play-pause"
  ];

  superShiftBinds = builtins.map (x: "SUPER SHIFT, " + x) [
    # Super+shift binds
    "S, exec, flameshot gui"
    "Print, exec, grim -g \"$(slurp)\""
    "ScrollDown, workspace, previous"
    "left , movewindow, l"
    "down , movewindow, d"
    "up , movewindow, u"
    "right , movewindow, r"
  ];
  superCtrlBinds = builtins.map (x: "SUPER CTRL, " + x) [
    # Super+shift binds
    "Space, workspace, previous"
    "S, layoutmsg, togglesplit"
    "equal, splitratio, 0.33"
    "minus, splitratio, -0.33"
  ];

  altBinds = builtins.map (x: "ALT, " + x) [
    # Alt binds
  ];

  shiftBinds = builtins.map (x: "SHIFT, " + x) [
    # Shift binds
  ];

  ctrlBinds = builtins.map (x: "CTRL, " + x) [
    # Ctrl binds
  ];

  ctrlShiftBinds = builtins.map (x: "CTRL SHIFT, " + x) [
    # Ctrl+shift binds
    "1, movetoworkspacesilent, 1"
    "2, movetoworkspacesilent, 2"
    "3, movetoworkspacesilent, 3"
    "4, movetoworkspacesilent, 4"
    "End, exec, shutdown now"
  ];
in
superBinds
++ superShiftBinds
++ superCtrlBinds
++ altBinds
++ ctrlBinds
++ ctrlShiftBinds
++ shiftBinds
# Docs
# https://wiki.hyprland.org/Getting-Started/Master-Tutorial/
