{ flake, ... }:
let

  inherit (flake.config.aesthetics.themes)
    currentTheme
    palettes
    ;

  el = palettes.${currentTheme}.colours;
in
{
  gaps_in = 5;
  gaps_out = 5;
  border_size = 2;
  "col.active_border" = "rgb(${el.base0E})";
  "col.inactive_border" = "0xff${el.base02}";

}
