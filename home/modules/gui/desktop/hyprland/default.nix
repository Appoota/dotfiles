{
  flake,
  config,
  ...
}:
let

  configPath = ./config;
  configImports = {
    animations = import (configPath + /animations.nix);
    bind = import (configPath + /bind.nix);
    bindm = import (configPath + /bindm.nix);
    binds = import (configPath + /binds.nix);
    decoration = import (configPath + /decoration.nix);
    dwindle = import (configPath + /dwindle.nix);
    exec-once = import (configPath + /exec-once.nix) {
      inherit
        flake
        ;
    };
    general = import (configPath + /general.nix) {
      inherit
        flake
        ;
    };
    input = import (configPath + /input.nix);
    misc = import (configPath + /misc.nix);
    windowrulev2 = import (configPath + /windowrulev2.nix);
  };
in
{
  wayland.windowManager.hyprland = {
    enable = true;
    # package = flake.inputs.hyprland.packages.${pkgs.system}.hyprland;
    xwayland.enable = true;
    systemd.enable = false;
    settings = configImports;
  };
}
