{
  flake,
  pkgs,
  ...
}:
{
  programs.wpaperd = {
    enable = true;
    package = flake.inputs.wpaperd.packages.${pkgs.system}.default;
    settings = {
      "default" = {
        path = "~/Files/Projects/dotfiles/home/modules/hypr/wpaperd/wallpaper";
        apply-shadow = true;
        duration = "1m";
        sorting = "random";
      };
    };
  };
}
