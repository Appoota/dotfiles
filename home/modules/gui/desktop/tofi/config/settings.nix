{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes)
    currentTheme
    palettes
    font
    ;

  el = palettes.${currentTheme}.colours;
in
{
  font-size = font.size.desktop;
  hint-font = true;
  background-color = "#${el.base02}";
  outline-color = "#${el.base0E}";
  prompt-color = "#${el.base0D}";
  input-color = "#${el.base07}";
  default-result-color = "#${el.base05}";
  selection-color = "#${el.base0C}";
  prompt-text = "Summon: ";

  width = 400;
  height = 400;

  outline-width = 1;
  border-width = 0;

  padding-top = 10;
  padding-bottom = 10;
  padding-left = 10;
  padding-right = 10;

  corner-radius = 10;

  drun-launch = true;
}
