{
  pkgs,
  flake,
  ...
}:
let
  configPath = ./config;

  settingsPath = import (configPath + /settings.nix) { inherit flake; };
in
{
  programs.tofi = {
    enable = true;
    settings = settingsPath;
  };
}
