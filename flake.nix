{
  description = "Nick's Big Fat Flakey Sausage";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-2.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    pre-commit-hooks-nix = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ghostty = {
      url = "github:ghostty-org/ghostty";
    };
    nixos-cosmic.url = "github:lilyinstarlight/nixos-cosmic";
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    systems.url = "github:nix-systems/x86_64-linux";
    helix = {
      url = "github:helix-editor/helix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland = {
      url = "github:hyprwm/Hyprland";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hypridle = {
      url = "github:hyprwm/hypridle";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    wpaperd = {
      url = "github:danyspin97/wpaperd";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    waybar = {
      url = "github:Alexays/Waybar";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    direnv = {
      url = "github:direnv/direnv";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.pre-commit-hooks-nix.flakeModule
        ./config
        ./home
        ./lib
        ./nixos
        ./parts
      ];

      flake =
        { config, ... }:
        {
          nixosConfigurations = {
            desktop = inputs.self.lib.mkLinuxSystem [
              ./system
              ./profile
              config.nixosModules.nixos
              inputs.home-manager.nixosModules.home-manager
              inputs.lix-module.nixosModules.default
              inputs.nur.modules.nixos.default
              inputs.nixos-cosmic.nixosModules.default
            ];
          };
          templates = {
            c = {
              path = ./templates/c;
              description = "C Environment";
            };
            haskell = {
              path = ./templates/haskell;
              description = "Haskell Environment";
            };
            julia = {
              path = ./templates/julia;
              description = "Julia Environment";
            };
            julia2 = {
              path = ./templates/julia2;
              description = "Julia Environment 2";
            };
            latex = {
              path = ./templates/latex;
              description = "Latex Environment";
            };
            r = {
              path = ./templates/r;
              description = "R Environment";
            };
            typst = {
              path = ./templates/typst;
              description = "Typst Environment";
            };
          };

        };
      systems = import inputs.systems;
    };
}
