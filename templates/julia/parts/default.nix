{
  perSystem = {
    imports = [
      ./devshells.nix
      ./pre-commit.nix
      ./treefmt.nix
    ];
  };
}
