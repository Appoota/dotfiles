{
  treefmt = {
    flakeCheck = false;
    programs = {
      nixpkgs-fmt.enable = true;
    };
    projectRootFile = "flake.nix";
  };
}
