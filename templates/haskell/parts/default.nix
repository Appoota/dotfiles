{
  perSystem = {
    pkgs,
    lib,
    config,
    self',
    ...
  }: {
    imports = [
      ./devshells.nix
      ./packages.nix
      ./pre-commit.nix
    ];
  };
}
