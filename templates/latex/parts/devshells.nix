{
  pkgs,
  config,
  ...
}: {
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit
        (pkgs)
        alejandra
        nil
        texliveFull
        ;
      inherit (pkgs.nodePackages) "@commitlint/config-conventional";
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
