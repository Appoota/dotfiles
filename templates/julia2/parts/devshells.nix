{
  pkgs,
  config,
  ...
}: {
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit (pkgs.julia.withPackages.override {precompile = false;} ["Plots" "JSON3" "Statistics" "HypothesisTests"]) julia;
      inherit (pkgs) hello;
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
