let
  modulesPath = ./modules;
  moduleImport =
    path: nameTransform:
    builtins.listToAttrs (
      map
        (name: {
          name = nameTransform name;
          value = import (path + "/${name}");
        })
        (
          builtins.filter (
            name: (builtins.readDir path).${name} == "regular" && builtins.match ".*\\.nix$" name != null
          ) (builtins.attrNames (builtins.readDir path))
        )
    );
  modules =
    moduleImport modulesPath (name: builtins.replaceStrings [ ".nix" ] [ "" ] name)
    // (
      path:
      builtins.listToAttrs (
        map
          (name: {
            name = name;
            value = import (path + "/${name}");
          })
          (
            builtins.filter (name: (builtins.readDir path).${name} == "directory") (
              builtins.attrNames (builtins.readDir path)
            )
          )
      )
    )
      modulesPath;
in
{
  flake.nixosModules = {
    nixos = {
      imports = builtins.attrValues {
        inherit (modules)
          android
          audio
          bluetooth
          dconf
          firejail
          flatpak
          fonts
          printing
          steam
          sysstat
          virtualization
          syncthing
          ollama
          # plasma
          # sddm
          hyprland
          accounts
          doas
          environment
          tablet
          home-manager
          locale
          nh
          nix
          rsyncd
          ssh
          system
          xserver
          usb
          ;
      };
    };
  };
}
