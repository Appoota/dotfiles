{
  services = {
    syncthing = {
      enable = true;
      overrideDevices = false;
      overrideFolders = false;
      openDefaultPorts = true;
      systemService = true;
      guiAddress = "127.0.0.1:8388";
      settings = {
        devices = {
        };
      };
    };
  };

  networking = {
    firewall = {
      allowedTCPPorts = [
        8388 # Syncthing (WebUI)
        21027 # Syncthing (Discovery)
        22000 # Syncthing (Transfer)
      ];
    };
  };
}
