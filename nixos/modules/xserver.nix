{
  services = {
    xserver = {
      enable = true;
      xkb = {
        layout = "us";
        # variant = "colemak_dh"
      };
    };
    libinput = {
      enable = true;
      touchpad = {
        tapping = true;
        naturalScrolling = false;
      };
      mouse.accelProfile = "flat";
      touchpad.accelProfile = "flat";
    };
  };
  console.useXkbConfig = true;
}
