{pkgs, ...}: {
  programs = {
    steam = {
      enable = true;
      remotePlay.openFirewall = true;
      dedicatedServer.openFirewall = true;
      extraPackages = builtins.attrValues {
        inherit
          (pkgs)
          curl
          ;
      };
    };
    java.enable = true;
  };
  hardware.steam-hardware.enable = true;
}
