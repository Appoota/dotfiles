{flake, ...}: let
  inherit
    (flake.config.people)
    user0
    ;
in {
  security = {
    doas = {
      enable = true;
      extraRules = [
        {
          keepEnv = true;
          noPass = true;
          users = [
            user0
          ];
        }
      ];
    };
    # sudo.enable = false;
  };
}
