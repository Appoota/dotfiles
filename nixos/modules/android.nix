{pkgs, ...}: {
  programs = {
    adb.enable = true;
    droidcam.enable = true;
  };
  services.udev.packages = builtins.attrValues {
    inherit
      (pkgs)
      android-udev-rules
      ;
  };
}
