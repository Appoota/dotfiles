{
  services = {
    ollama = {
      acceleration = false;
      enable = true;
      group = "ollama";
      host = "http://127.0.0.1";
      models = "/mnt/media/storage/ollama";
      user = "ollama";
    };
    open-webui = {
      enable = true;
      host = "127.0.0.1";
      port = 8088;
      environment = {
        ENABLE_OLLAMA_API = "True";
        ANONYMIZED_TELEMETRY = "False";
        DO_NOT_TRACK = "True";
        SCARF_NO_ANALYTICS = "True";
        OLLAMA_BASE_URL = "http://127.0.0.1:11434";
        WEBUI_AUTH = "True";
      };
    };
  };

  systemd.tmpfiles.rules = [
    "Z /mnt/media/storage/ollama 0755 ollama ollama -"
  ];

  networking = {
    firewall = {
      allowedTCPPorts = [
        8088 # Open-WebUI (Ollama Front End)
        11434 # Ollama API
      ];
    };
  };
}
