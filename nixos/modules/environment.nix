{
  pkgs,
  ...
}:
{
  environment = {
    # enableAllTerminfo = true;
    systemPackages = builtins.attrValues {
      inherit (pkgs)
        git
        grim
        pijul
        sshfs
        tomb
        just
        virt-manager
        ;
    };
    variables = {
      VIDEO_PLAYER = "mpv";
      EDITOR = "hx";
      TERM = "ghostty";
      WLR_NO_HARDWARE_CURSORS = "1";
      WLR_DRM_NO_ATOMIC = "1";
      NIXPKGS_ALLOW_INSECURE = "1";
      NIXPKGS_ALLOW_UNFREE = "1";
      FLAMESHOT_ENABLE_WAYLAND = "1";
      USE_WAYLAND_GRIM = "1";
      USE_WAYLAND_CLIPBOARD = "1";
      GTK_THEME = "catppuccin-mocha-blue-compact";
    };
  };
}
