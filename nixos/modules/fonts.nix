{
  pkgs,
  flake,
  ...
}:
let
  inherit (flake.config.aesthetics.themes)
    font
    ;
in
{
  fonts = {
    fontconfig = {
      enable = true;
      defaultFonts = {
        serif = [
          font.name
        ];
        monospace = [
          font.name
        ];
        sansSerif = [
          font.name
        ];
        emoji = [
          "Noto Fonts Color Emoji"
        ];
      };
      antialias = true;
    };
    packages = builtins.attrValues {
      inherit (pkgs)
        noto-fonts-color-emoji
        dosis
        iosevka
        ;
      inherit (pkgs.nerd-fonts)
        monaspace
        fantasque-sans-mono
        ;
    };
  };
}
