#{pkgs, ...}:
{
  #  hardware.printers = {
  #    ensurePrinters = [
  #      {
  #        name = "";
  #        location = "";
  #        deviceUri = "";
  #        model = "drv:///sample.drv/generic.ppd";
  #        ppdOptions = {
  #          PageSize = "Letter";
  #        };
  #      }
  #    ];
  #    ensureDefaultPrinter = "";
  #  };

  services.printing = {
    enable = true;
    drivers = [
      # drivers go here like:
      # pkgs.<driver package>
    ];
  };
}
