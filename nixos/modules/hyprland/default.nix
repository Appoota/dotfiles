{
  imports = map (file: ./${file}.nix) [
    "geoclue2"
    "greetd"
    "network"
    "hypridle"
    "hyprland"
    # "hyprlock"
    # "regreet"
    "swaylock"
    "thunar"
    "wayland"
  ];
}
