{
  pkgs,
  ...
}:
{
  programs.regreet = {
    enable = true;
    package = pkgs.greetd.regreet;
  };
}
