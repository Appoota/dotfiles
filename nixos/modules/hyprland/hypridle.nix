{
  flake,
  pkgs,
  ...
}: {
  services.hypridle = {
    enable = true;
    package = flake.inputs.hypridle.packages.${pkgs.system}.hypridle;
  };
}
