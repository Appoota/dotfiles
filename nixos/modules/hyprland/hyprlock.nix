{
  flake,
  pkgs,
  ...
}: {
  programs.hyprlock = {
    enable = true;
    package = flake.inputs.hyprlock.packages.${pkgs.system}.hyprlock;
  };
}
