{
  config,
  flake,
  pkgs,
  lib,
  ...
}:
let
  inherit (flake.config.people)
    user0
    ;
  inherit (flake.config.people.users.${user0})
    name
    paths
    ;
in
{
  users = {
    users.${user0} = {
      description = name;
      isNormalUser = true;
      shell = pkgs.nushell;
      extraGroups = [
        "adbusers"
        "caddy"
        "disk"
        "libvirtd"
        "netdev"
        "networkmanager"
        "ollama"
        "samba"
        "vboxusers"
        "wheel"
      ];
    };
  };
  home-manager.users = {
    ${user0} = {
      home = {
        username = user0;
        homeDirectory = "/home/${user0}";
        file = {
          "./justfile" = {
            source = ./files/justfile;
          };
          "./.config/vesktop/themes/macchiato-theme.css" = {
            source = ./files/themes/vesktop/macchiato-theme.css;
          };
        };
        sessionVariables = {
          VISUAL = lib.getExe pkgs.zed-editor;
          GTK_THEME = "catppuccin-mocha-blue-compact";
        };
      };
      imports = [
        {
          home.stateVersion = config.system.stateVersion;
        }
        (import ./config { flake = flake; })
      ];
    };
  };
  systemd.tmpfiles = {
    rules =
      [
        "d ${paths.path0} 0755 ${user0} users -"
      ]
      ++ (map (path: "d /home/${user0}/${path} 0755 ${user0} users -") [
        # Custom home folders you want to add
      ])
      ++ (map (path: "R /home/${user0}/${path} 0755 ${user0} users - -") [
        # Default home folders you want to delete
      ]);
  };
}
