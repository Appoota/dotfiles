{ flake, ... }:
let
  inherit (flake)
    self
    ;

  moduleImports = map (module: self.homeModules.${module}) [
    "desktop"
  ];
in
{
  imports = moduleImports;
}
