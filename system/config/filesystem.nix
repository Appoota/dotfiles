{
  flake,
  config,
  ...
}:
let
  inherit (flake.config.people)
    user0
    ;
in
{
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/07e12b6b-89a6-407d-9412-9f417f4f50a4";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/7755-0061";
      fsType = "vfat";
    };
  };

  swapDevices = [
  ];

  systemd.tmpfiles.rules = [
    "Z ${config.home-manager.users.${user0}.home.homeDirectory} 0755 ${user0} users -"
  ];

  services.udisks2.enable = true;
}
