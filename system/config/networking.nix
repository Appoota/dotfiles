{ lib, ... }:
{
  networking = {
    hostName = "Desktop";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };
  services = {
    avahi = {
      enable = true;
      openFirewall = true;
      nssmdns4 = true;
    };
    sshd.enable = true;
  };
}
